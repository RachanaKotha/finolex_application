var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, ModalController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { ProductsModalPage } from '../products-modal/products-modal';
import { AlertController } from 'ionic-angular';
import { ReedemapiProvider } from '../../providers/reedemapi/reedemapi';
import { Storage } from '@ionic/storage';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { NotificationsPage } from '../notifications/notifications';
var ProductsPage = /** @class */ (function () {
    function ProductsPage(navCtrl, navParams, modalCtrl, viewCtrl, alertCtrl, storage, reedemapi, loadingCtrl, iab) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.reedemapi = reedemapi;
        this.loadingCtrl = loadingCtrl;
        this.iab = iab;
        this.loaddata();
    }
    ProductsPage.prototype.loaddata = function () {
        var _this = this;
        this.load = this.loadingCtrl.create({
            content: 'Please Wait....',
        });
        this.load.present();
        this.storage.get('api_token').then(function (token) {
            _this.reedemapi.getProductCatalogues(token)
                .then(function (data) {
                _this.load.dismiss();
                console.log(data);
                var resp = data;
                if (resp['status'] == true) {
                    console.log(resp['data']);
                    _this.data = resp['data'];
                }
            }, function (error) {
                console.log(error);
            });
        });
    };
    ProductsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductsPage');
    };
    ProductsPage.prototype.showNotifications = function () {
        this.navCtrl.push(NotificationsPage);
    };
    ProductsPage.prototype.productClick = function (id) {
        console.log('item clicked', this.data[id]);
        this.navCtrl.push(ProductsModalPage, { productData: this.data[id] });
    };
    ProductsPage.prototype.openBrowser = function (product) {
        var browser = this.iab.create(product.link);
        browser.show();
    };
    ProductsPage = __decorate([
        Component({
            selector: 'page-products',
            templateUrl: 'products.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ModalController, ViewController, AlertController, Storage,
            ReedemapiProvider, LoadingController, InAppBrowser])
    ], ProductsPage);
    return ProductsPage;
}());
export { ProductsPage };
//# sourceMappingURL=products.js.map