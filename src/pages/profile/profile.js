var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { Storage } from '@ionic/storage';
import { ActionSheetController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { AlertController, ModalController } from 'ionic-angular';
import { Events } from 'ionic-angular';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams, _form, user, storage, actionSheetCtrl, camera, alertCtrl, toastCtrl, modalCtrl, events) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._form = _form;
        this.user = user;
        this.storage = storage;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.modalCtrl = modalCtrl;
        this.events = events;
        this.defaultImage = "https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?auto=compress&cs=tinysrgb&h=350";
        this.profile_page = this._form.group({
            name: ['', Validators.compose([Validators.required])],
            gender: ['', Validators.compose([Validators.required])],
            date_of_birth: ['', Validators.compose([Validators.required])],
            address: ['', Validators.compose([Validators.required])],
            city: ['', Validators.compose([Validators.required])],
            state: ['', Validators.compose([Validators.required])],
            pincode: ['', Validators.compose([Validators.required, Validators.maxLength(6), Validators.minLength(6)])],
            api_token: ['']
        });
        this.updateDataOnPage();
        this.storage.get('api_token').then(function (val) {
            _this.profile_page.controls.api_token.setValue(val);
        });
        this.isReadonly = true;
        this.isReadPic = true;
        this.isSubmit = false;
    }
    ProfilePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProfilePage');
    };
    ProfilePage.prototype.edit = function () {
        console.log(this.isReadonly);
        if (this.isReadonly == true) {
            this.isReadonly = false;
            console.log(this.isReadonly);
        }
        else {
            this.isReadonly = true;
            console.log(this.isReadonly);
        }
        this.isSubmit = !this.isReadonly;
    };
    ProfilePage.prototype.editImage = function () {
        console.log(this.isReadPic);
        if (this.isReadPic == true) {
            console.log(this.isReadPic);
            this.isReadPic = false;
        }
        else {
            console.log(this.isReadPic);
            this.isReadPic = true;
        }
    };
    ProfilePage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'TAKE PHOTO',
            buttons: [
                {
                    text: 'Camera',
                    handler: function () {
                        console.log('camera clicked');
                        _this.takePhoto(1);
                    }
                },
                {
                    text: 'Gallery',
                    handler: function () {
                        console.log('Gallery clicked');
                        _this.takePhoto(0);
                    }
                }
            ]
        });
        actionSheet.present();
    };
    ;
    ProfilePage.prototype.takePhoto = function (val) {
        var _this = this;
        var options = {
            quality: 30,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            sourceType: val,
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log(base64Image);
            _this.updateProfileImage(base64Image);
        }, function (err) {
            // Handle error
        });
    };
    ProfilePage.prototype.updateProfileImage = function (img) {
        var _this = this;
        this.storage.get('api_token').then(function (val) {
            _this.user.updateImg(val, img).then(function (dataSet) {
                if (dataSet['status'] == true) {
                    console.log(dataSet);
                    var imageData = dataSet['data'];
                    _this.image = imageData['image'];
                    console.log(_this.image);
                    _this.profileDetail();
                    _this.presentAlert(dataSet['message']);
                    _this.storage.set('image', imageData['image']);
                    //this.navCtrl.setRoot(DashboardPage);
                }
                else {
                    /* let toast = this.toastCtrl.create({
                      message: dataSet['message'],
                      duration: 3000,
                      position: 'top'
                    });

                    toast.present();*/
                    _this.presentAlert(dataSet['message']);
                }
            });
        });
        this.isReadPic = true;
        this.navCtrl.setRoot(this.navCtrl.getActive().component);
    };
    ProfilePage.prototype.editProfile = function () {
        var _this = this;
        console.log(this.isReadonly);
        this.user.updateProfile(this.profile_page.getRawValue()).then(function (dataSet) {
            console.log(dataSet);
            console.log(dataSet['status']);
            if (dataSet['status'] == true) {
                _this.storage.set('profile', dataSet['data']).then(function (data) {
                    console.log(data);
                    _this.updateDataOnPage();
                    _this.isReadonly = true;
                    console.log(_this.isReadonly);
                });
                console.log(dataSet['data']['profile_details']['name']);
                _this.profile_page.controls.name.setValue(dataSet['data']['profile_details']['name']);
            }
        });
    };
    ProfilePage.prototype.profileDetail = function () {
        var _this = this;
        this.storage.get('api_token').then(function (val) {
            _this.user.getProfileData(val).then(function (dataSet) {
                console.log(dataSet);
                _this.profileData = dataSet['data'];
                _this.storage.set('profile', dataSet['data']).then(function (data) {
                    _this.storage.get('profile').then(function (val) {
                        console.log(val);
                        _this.storage.set('user_name', val.profile_details.name);
                        _this.profile_page.controls.name.setValue(val.profile_details.name);
                        _this.profile_page.controls.gender.setValue(val.profile_details.gender);
                        _this.profile_page.controls.date_of_birth.setValue(val.profile_details.date_of_birth);
                        _this.profile_page.controls.address.setValue(val.profile_details.address);
                        _this.profile_page.controls.city.setValue(val.profile_details.city);
                        _this.profile_page.controls.state.setValue(val.profile_details.state);
                        _this.profile_page.controls.pincode.setValue(val.profile_details.pincode);
                        _this.image = val.profile_details.image;
                        if (_this.image == null) {
                            _this.image = "https://images.pexels.com/photos/39811/pexels-photo-39811.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940";
                        }
                        _this.name = val.profile_details.name;
                    });
                });
                console.log(_this.profileData);
            });
        });
    };
    ProfilePage.prototype.updateDataOnPage = function () {
        var _this = this;
        this.storage.get('profile').then(function (val) {
            console.log(val);
            _this.storage.set('user_name', val.profile_details.name);
            _this.profile_page.controls.name.setValue(val.profile_details.name);
            _this.profile_page.controls.gender.setValue(val.profile_details.gender);
            _this.profile_page.controls.date_of_birth.setValue(val.profile_details.date_of_birth);
            _this.profile_page.controls.address.setValue(val.profile_details.address);
            _this.profile_page.controls.city.setValue(val.profile_details.city);
            _this.profile_page.controls.state.setValue(val.profile_details.state);
            _this.profile_page.controls.pincode.setValue(val.profile_details.pincode);
            _this.image = val.profile_details.image;
            if (_this.image == null) {
                _this.image = "assets/images/Blank_Avatar.png";
            }
            _this.name = val.profile_details.name;
            _this.events.publish('user:created', 'user', Date.now());
        });
    };
    ProfilePage.prototype.presentAlert = function (message) {
        var _this = this;
        var alert = this.alertCtrl.create({
            subTitle: message,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                        console.log('Ok clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    ProfilePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-profile',
            templateUrl: 'profile.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams,
            FormBuilder, UserserviceProvider,
            Storage, ActionSheetController,
            Camera, AlertController,
            ToastController, ModalController,
            Events])
    ], ProfilePage);
    return ProfilePage;
}());
export { ProfilePage };
//# sourceMappingURL=profile.js.map