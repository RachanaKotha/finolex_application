var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
/**
 * Generated class for the ModalpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ModalpagePage = /** @class */ (function () {
    function ModalpagePage(navCtrl, navParams, viewctrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewctrl = viewctrl;
        this.alertCtrl = alertCtrl;
        this.page = navParams.get('page');
        console.log('data', this.page);
    }
    ModalpagePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ModalpagePage');
    };
    ModalpagePage.prototype.dismiss = function () {
        this.viewctrl.dismiss();
    };
    ModalpagePage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Dates',
            subTitle: '10% of battery remaining',
            inputs: [
                {
                    name: 'from Date',
                    placeholder: 'From Date',
                    type: 'date',
                    id: 'from_date'
                },
                {
                    name: 'To date',
                    placeholder: 'To Date',
                    type: 'date'
                }
            ],
            buttons: ['Dismiss']
        });
        alert.present();
    };
    ModalpagePage.prototype.submit = function () {
        this.data = { fromDate: this.fromdate, toDate: this.todate };
        this.viewctrl.dismiss(this.data);
    };
    ModalpagePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-modalpage',
            templateUrl: 'modalpage.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ViewController, AlertController])
    ], ModalpagePage);
    return ModalpagePage;
}());
export { ModalpagePage };
//# sourceMappingURL=modalpage.js.map