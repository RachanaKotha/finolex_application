import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Market } from '@ionic-native/market';


@IonicPage()
@Component({
  selector: 'page-forceupdate',
  templateUrl: 'forceupdate.html',
})
export class ForceupdatePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private market : Market) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForceupdatePage');
  }

	gotoPlaystore(){
		this.market.open("com.finolex.finolexapp");
	}
}
