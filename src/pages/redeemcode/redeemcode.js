var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AlertController, ModalController, ToastController } from 'ionic-angular';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { Storage } from '@ionic/storage';
var RedeemcodePage = /** @class */ (function () {
    function RedeemcodePage(navCtrl, navParams, modalCtrl, barcodeScanner, user, alertCtrl, storage, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.barcodeScanner = barcodeScanner;
        this.user = user;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.scannedCodes = [];
        this.i = 0;
        this.k = 0;
        this.l = 0;
        this.flag = 0;
        this.redeemcode = '';
        this.codeAddition = [];
        this.isScanned = false;
    }
    RedeemcodePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RedeemcodePage');
    };
    RedeemcodePage.prototype.scan = function () {
        var _this = this;
        this.flag = 0;
        this.barcodeScanner.scan().then(function (barcodeData) {
            console.log('Barcode data', barcodeData);
            console.log(barcodeData.text);
            if (barcodeData.text != '') {
                if (_this.i > 0) {
                    _this.isScanned = true;
                    for (var j = 0; j <= _this.i; j++) {
                        if (_this.scannedCodes[j] == barcodeData.text) {
                            _this.flag += 1;
                            alert("u already scanned this code");
                        }
                    }
                    console.log(_this.flag);
                    if (_this.flag == 0) {
                        _this.scannedCodes[_this.i] = barcodeData.text;
                        _this.i += 1;
                    }
                }
                else {
                    _this.scannedCodes[_this.i] = barcodeData.text;
                    _this.i += 1;
                    _this.isScanned = true;
                }
            }
            console.log(_this.scannedCodes);
            console.log(_this.scannedCodes.toString());
            _this.redeemcode = _this.scannedCodes.toString();
        }).catch(function (err) {
            console.log('Error', err);
        });
    };
    RedeemcodePage.prototype.addCode = function () {
        console.log("in adding");
        this.codeAddition = [];
        this.codeAddition = this.redeemcode.split(',');
        console.log(this.codeAddition);
        console.log(this.codeAddition.length);
        this.scannedCodes = [];
        for (this.k = 0; this.k < this.codeAddition.length; this.k++) {
            if (this.codeAddition[this.k] == '') {
                for (this.l = this.k + 1; this.l < this.codeAddition.length; this.l++) {
                    this.codeAddition[this.k] = this.codeAddition[this.l];
                }
            }
        }
        console.log(this.codeAddition);
        for (this.k = 0; this.k < this.codeAddition.length; this.k++) {
            if (this.codeAddition[this.k] != '') {
                this.scannedCodes[this.k] = this.codeAddition[this.k];
            }
        }
        console.log(this.scannedCodes);
        this.i = this.scannedCodes.length;
        if (this.codeAddition.length == 1) {
            if (this.codeAddition[0] == '') {
                this.i = 0;
            }
        }
        if (this.i > 0) {
            this.isScanned = true;
        }
        if (this.i == 0) {
            this.isScanned = false;
        }
    };
    RedeemcodePage.prototype.submitCode = function () {
        var _this = this;
        this.storage.get('api_token').then(function (token) {
            _this.storage.get('mobile_no').then(function (mobile) {
                _this.user.redeemCoupon(token, mobile, _this.scannedCodes).then(function (res) {
                    console.log(res); /*
                    let toast=this.toastCtrl.create({
                        message: res['message'],
                        duration: 3000,
                        position: 'top'
                    });
                    toast.present();*/
                    _this.scannedCodes = [];
                    _this.redeemcode = '';
                    _this.i = 0;
                    _this.codeAddition = [];
                    _this.flag = 0;
                    _this.isScanned = false;
                    _this.presentAlert(res['message']);
                });
            });
        });
    };
    RedeemcodePage.prototype.presentAlert = function (message) {
        var _this = this;
        var alert = this.alertCtrl.create({
            subTitle: message,
            buttons: [
                {
                    text: 'Ok',
                    handler: function () {
                        _this.navCtrl.setRoot(_this.navCtrl.getActive().component);
                        console.log('Ok clicked');
                    }
                }
            ]
        });
        alert.present();
    };
    RedeemcodePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-redeemcode',
            templateUrl: 'redeemcode.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, ModalController,
            BarcodeScanner, UserserviceProvider, AlertController,
            Storage, ToastController])
    ], RedeemcodePage);
    return RedeemcodePage;
}());
export { RedeemcodePage };
//# sourceMappingURL=redeemcode.js.map