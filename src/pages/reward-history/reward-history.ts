import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RewardProvider} from '../../providers/reward/reward';
import { AlertController,ModalController } from 'ionic-angular';
import { ModalpagePage } from '../modalpage/modalpage';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'page-reward-history',
  templateUrl: 'reward-history.html',
})
export class RewardHistoryPage {
	dataSet : any;
modalData : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public rewardapi : RewardProvider,public alertCtrl: AlertController,
    public modalCtrl : ModalController,public storage:Storage,public datepipe: DatePipe) {

   this.getRewardHistory();
  }

  getRewardHistory()
  {
     var date=new Date();
      var fromdate=this.datepipe.transform(date, 'yyyy-MM-dd');
      var todate=this.datepipe.transform(date, 'yyyy-MM-dd');
      if(this.modalData!= undefined){
         fromdate=this.modalData['fromDate'];
         todate=this.modalData['toDate'];
         if(fromdate == undefined || todate == undefined){
            fromdate='';
            todate='';
         }
      }else{
        fromdate='';
        todate='';
      }
      this.storage.get('api_token').then(token=>{

        this.rewardapi.getRewardHistoryList(token,fromdate,todate).then(data2=>{
          console.log(data2);
          this.dataSet=data2['data'];
        });
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RewardHistoryPage');
  }
  presentAlert(){
  let modal = this.modalCtrl.create(ModalpagePage,{page:RewardHistoryPage});
    modal.present();

    modal.onDidDismiss(data => {
      this.modalData=data;
      if(this.modalData != undefined){
        if(this.modalData.fromDate!=''&&this.modalData.toDate!=''){
      console.log(this.modalData.fromDate);
      console.log(this.modalData.toDate);
     console.log(data);
      }
    }
    this.getRewardHistory();
});
  }

}
