import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { UserserviceProvider } from '../../providers/userservice/userservice';
import { Storage } from '@ionic/storage';
import {DashboardPage} from '../dashboard/dashboard';
import { ToastController } from 'ionic-angular';
import { AlertController,ModalController} from 'ionic-angular';
import {ForgotpasswordProvider} from '../../providers/forgotpassword/forgotpassword';
import {OtpPage} from '../otp/otp';


@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
	oldPassword;
	newPassword;
	confirmPassword;
	data;isConfirm;
  checkPass;
  oldpasswordType: string = 'password';
  oldpasswordIcon: string = 'eye-off'; 
  newpasswordType: string = 'password';
  newpasswordIcon: string = 'eye-off'; 
  confirmpasswordType: string = 'password';
  confirmpasswordIcon: string = 'eye-off';
  loader : any;


  constructor(public navCtrl: NavController, public navParams: NavParams, 
  				public user: UserserviceProvider,private storage: Storage,
          private loaderController : LoadingController,
          private forgotProvider : ForgotpasswordProvider,
          private toastCtrl: ToastController,public alert: AlertController) {
        this.oldPassword='';
        this.newPassword='';
        this.confirmPassword='';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }
  changePassword(){

    this.loader= this.loaderController.create({
            content:'Please Wait....',
          });
    this.storage.get('mobile_no').then((val) => {
          this.loader.present();

//          console.log(this.form.value['mobile_no']);

          
      this.forgotProvider.getForgotPassword(val)
        .then(data => {

          this.loader.dismiss();

          if(data['status']==true){

            this.data={
              page : "ChangePasswordPage",
              mobile_no:val,
        old_password: this.oldPassword,
        new_password: this.newPassword,
        confirm_password: this.confirmPassword
      }
      console.log('otp',this.data);
           
           this.navCtrl.push(OtpPage,{data:this.data});
          }
          else{
          let alertCtrl = this.alert.create({
              title: 'Finolex',
              subTitle: data['message'],
              buttons: ['OK']
            });
            alertCtrl.present();          
          }
            console.log(data);
        });

      });


  	/*this.data={};
    this.storage.get('api_token').then((val)=>{
  	if(this.newPassword==this.confirmPassword){
  		this.data={
  			old_password: this.oldPassword,
  			new_password: this.newPassword,
  			confirm_password: this.confirmPassword,
        api_token: val 
  		}
  		this.user.changePassword(this.data).then(dataset=>{
  			 console.log(dataset);
         console.log(dataset['status']);
         this.presentAlert(dataset['message']);
        var message=dataset['message'];

         if(dataset['status']==true){

              let alert = this.alertCtrl.create({
                title: 'Finolex',
                message: message,
                buttons: [
                  {
                    text: 'Ok',
                    role: 'cancel',
                    handler: () => {
                      console.log('Cancel clicked');
                       this.navCtrl.setRoot(DashboardPage);
                    }
                  }
                 ]              
              });
              
              alert.present();            
         }else{
          
         }
         let toast = this.toastCtrl.create({
              message: dataset['message'],
              duration: 3000,
              position: 'top'
            });
         toast.present();

  		});
  	}
    else{

    	this.presentAlert('new Password and confirm password should be same');

       //console.log("new Password and confirm password should be same");
    }
  });*/

  }
  validatePassword(){
    if(this.newPassword == this.confirmPassword){
      this.isConfirm=true;
      this.checkPass=true;
      console.log(this.isConfirm);
    }
    else{
      console.log(this.isConfirm);
      this.checkPass=false;
      this.isConfirm=false;
    }
    
  }

 hideShowPassword(val) {

  if(val=='Old'){
     this.oldpasswordType = this.oldpasswordType === 'text' ? 'password' : 'text';
     this.oldpasswordIcon = this.oldpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }else if(val=='New'){
     this.newpasswordType = this.newpasswordType === 'text' ? 'password' : 'text';
     this.newpasswordIcon = this.newpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  else if(val == 'Confirm'){
     this.confirmpasswordType = this.confirmpasswordType === 'text' ? 'password' : 'text';
     this.confirmpasswordIcon = this.confirmpasswordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  
 }
 presentAlert(message) {
    let alert = this.alert.create({
      
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.setRoot(this.navCtrl.getActive().component);
            console.log('Ok clicked');
          }
        }
      ]
    });
    alert.present();
  }
}
