var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { Http, Headers } from '@angular/http';
/*
  Generated class for the ReedemapiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ReedemapiProvider = /** @class */ (function () {
    function ReedemapiProvider(http) {
        this.http = http;
        this.url = 'http://app.meltag.com/finolex/api/';
        console.log('Hello ReedemapiProvider Provider');
        this._header = new Headers();
        this._header.append('finolex-user', 'finolex@meltag.io');
        this._header.append('finolex-password', 'finolex$meltag@123');
    }
    ReedemapiProvider.prototype.getReedemHistory = function (token, offset, from, to) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log(token);
            _this.http.get(_this.url + 'redeem/history?api_token=' + token + '&offset=' + offset + '&limit=20&from_date=' + from + '&to_date=' + to, { headers: _this._header }).toPromise().then(function (data) {
                console.log(data);
                resolve(data.json());
            }, function (err) {
                console.log(err);
                reject(err);
            });
        });
    };
    ReedemapiProvider.prototype.getProductCatalogues = function (token) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            console.log(token);
            _this.http.get(_this.url + 'products?api_token=' + token, { headers: _this._header }).toPromise().then(function (data) {
                console.log(data);
                resolve(data.json());
            }, function (err) {
                console.log(err);
                reject(err);
            });
        });
    };
    ReedemapiProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http])
    ], ReedemapiProvider);
    return ReedemapiProvider;
}());
export { ReedemapiProvider };
//# sourceMappingURL=reedemapi.js.map